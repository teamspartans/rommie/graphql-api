"""Staging file configuration.

Simple configuration file.
"""
from core.common_cloud_settings import *

DEBUG = True
PRODUCTION = False
CLOUD = True
CORS_ORIGIN_ALLOW_ALL = True