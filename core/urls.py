"""core URL Configuration.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/

Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# pylint: disable=E0401
# pylint: disable=C0103
# pylint: disable=W0611
from django.conf import settings
from django.conf.urls import handler404
from django.conf.urls import handler500
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from graphene_file_upload.django import FileUploadGraphQLView

from .schema import schema

handler404 = 'core.views.handler404'
handler500 = 'core.views.handler404'

urlpatterns = [
    # GraphQL Path
    path(
        'graphql',
        csrf_exempt(
            FileUploadGraphQLView.as_view(
                graphiql=not settings.PRODUCTION, schema=schema
            )
        ),
    ),
    # Admin Path
    path('admin/', admin.site.urls),
    # Kumpel API
    path(
        '', include(('kumpel_api.urls', 'kumpel_api'), namespace='kumpel_api')
    ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
