<div align="center">

<h3>API GraphQL</h3>

An API to provide data for the frontend application

  <img src="core/img/logo.png" height="100px">
</div>

---

<div align="center">
	<img src="https://img.shields.io/badge/License-GPLv3-blue.svg"  alt="license badge">
    <img src="https://img.shields.io/pypi/pyversions/Django.svg?style=flat-square"  alt="python badge">
    <img src="https://img.shields.io/pypi/v/django.svg?label=django" alt="django badge">
    <img src="https://img.shields.io/pypi/v/graphene.svg?label=graphene" alt="django badge">
    <img src="https://img.shields.io/pypi/v/coverage.svg?label=coverage" alt="django badge">
</div>
<div align="center">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/eocode?label=eocode&style=social">
    <img alt="GitHub followers" src="https://img.shields.io/github/followers/KorKux1?label=KorKux1&style=social">
</div>
<div align="center">
    <img alt="Twitter URL" src="https://img.shields.io/twitter/url?label=eocode&style=social&url=https%3A%2F%2Ftwitter.com%2Feocode">
</div>

## Demo

If you want to see the demo of this project deployed, you can visit it here

Production enviroment

https://kumpel-282121.uc.r.appspot.com/

Staging enviroment

https://ideas-staging-282118.wn.r.appspot.com/

Up until July 2020

## How to clone
You can clone the repository

    $ git clone git@gitlab.com:teamspartans/rommie/graphql-api.git

## Installation
To install this project just type

Create virtual enviroment:

    $ python -m venv env

Active your enviroment

Install dependencies

    $ pip install -r requirements.txt
    $ pip install -r core/requirements/development.txt

Configure enviroment

    $ python core/enviroments/config.py ``development``

Compile statics

    $ python manage.py collectstatic

Run the project

    $ python manage.py runserver

Validate your code on push commit

    $ pre-commit install

For seed Data
- Create .env on root project

- On .ev file write

  ```
  PASSWORD=[Password]
  ```

## File structure

* **core** (Main module and configuration for project)
  * **enviroments** (Files to build ci/cd)
  * **img** (Readme images for project)
  * **requirements** (python dependencies for development and testing)
  * **static** (Static files for Django app)
  * **templates** (Override original template files)
* **kumpel_api**
  * **users** users module
  * **rooms** rooms module
  * **utils** common api utils module

## Development configuration

On root folder create a env.yaml file

```yaml
env_variables:
  host: yourhost
  user: yourusername
  password: yourpassword
  database: yourdatabasename
test_variables:
  password: yoursecurepasswordfortesting
```

```shell
python manage.py migrate
```

## Production enviroment

* Merge or send a pull request on master for deploy

## Preview

### Kumpel admin panel

<div align="center">
  <img src="core/img/admin.png">
</div>

### Kumpel administrators access

<div align="center">
  <img src="core/img/main_site.png">
</div>

### API with docs

<div align="center">
  <img src="core/img/api.png">
</div>

### Gitlab Realtime status project enviroments

<div align="center">
  <img src="core/img/status.png">
</div>

## How to contribute

* Review the [code of conduct](https://gitlab.com/teamspartans/code-of-conduct) to contribute to the project
* You can create a Pull request to the project

## License

GNU GENERAL PUBLIC LICENSE
Version 3
