"""Kumpel API Views.

Show the main page application.
"""
from django.shortcuts import render

from core.enviroments.connect import generate_main_context_view
from kumpel_api.utils.seed_data import clear_room_models
from kumpel_api.utils.seed_data import clear_user_models
from kumpel_api.utils.seed_data import generate_catalogs
from kumpel_api.utils.seed_data import generate_rooms_fake_data
from kumpel_api.utils.seed_data import generate_users_fake_data
from kumpel_api.utils.utils import reduce_avatars
from kumpel_api.utils.utils import reduce_rooms


def main_page(request):
    """View for link app.

    Link app with admin, stats and api.
    """
    context = generate_main_context_view()
    return render(request, 'kumpel/index.html', context)


def clear_data(request):
    """Generate catalogs for app."""
    clear_room_models()
    clear_user_models()
    return render(request, 'kumpel/clean.html')


def catalogs(request):
    """Update or create catalogs."""
    generate_catalogs()
    return render(request, 'kumpel/catalogs.html')


def generate_users(request):
    """Clear data from app."""
    generate_users_fake_data(10)
    return render(request, 'kumpel/seed.html')


def generate_rooms(request):
    """Clear data from app."""
    generate_rooms_fake_data()
    return render(request, 'kumpel/seed.html')


def reduce_avatar_images(request):
    """Reduce size of the user avatar."""
    reduce_avatars()
    return render(request, 'kumpel/seed.html')


def reduce_rooms_images(request):
    """Reduce images sizes of the rooms."""
    reduce_rooms()
    return render(request, 'kumpel/seed.html')
