"""Admin Site Configuration.

Setup admin settings.
"""
from django.contrib import admin

# Project configurations
admin.site.site_header = 'Kumpel'
admin.site.site_title = 'Kumpel'
admin.site.index_title = 'Spartans'
admin.empty_value_display = '**Vacio**'
