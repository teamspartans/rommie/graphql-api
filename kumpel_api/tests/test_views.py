"""Test views of Kumepl API."""
from django.test import TestCase

# pylint: disable=too-few-public-methods


class TestCallsViews(TestCase):
    """Test main view of API."""

    def test_main_page_view_load(self):
        """Test if main page loads."""
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'kumpel/index.html')
