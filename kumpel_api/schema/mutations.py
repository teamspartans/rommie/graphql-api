"""Api mutations.

Create all mutations for API.
"""
import graphene

from kumpel_api.schema.type_favorite_rooms import CreateFavoriteRoom
from kumpel_api.schema.type_room import CreateRoom
from kumpel_api.schema.type_room import DeleteRoom
from kumpel_api.schema.type_room import UnDeleteRoom
from kumpel_api.schema.type_user import EditProfileMutation
from kumpel_api.schema.type_user import KumpelLogin
from kumpel_api.schema.type_user import KumpelLogout
from kumpel_api.schema.type_user import KumpelRegister

# pylint: disable=too-few-public-methods
# pylint: disable=no-self-argument
# pylint: disable=no-self-use
# pylint: disable=unused-argument
# pylint: disable=no-member


class Mutation(graphene.ObjectType):
    """Mutations for Kumpel API."""

    kumpel_login = KumpelLogin.Field()
    kumpel_logout = KumpelLogout.Field()
    kumpel_register = KumpelRegister.Field()
    room_create = CreateRoom.Field()
    room_favorites_add = CreateFavoriteRoom.Field()
    room_delete = DeleteRoom.Field()
    room_undelete = UnDeleteRoom.Field()
    profile_edit = EditProfileMutation.Field()
