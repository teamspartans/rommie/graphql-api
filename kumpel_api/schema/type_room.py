"""Room types.

All room Types for use on Querys and Mutations.
"""
import logging

import graphene
from django.core.exceptions import ObjectDoesNotExist
from graphene_django.types import DjangoObjectType
from graphene_file_upload.scalars import Upload

from kumpel_api.rooms.models import Feature
from kumpel_api.rooms.models import FeatureCategory
from kumpel_api.rooms.models import PlacesCategory
from kumpel_api.rooms.models import Room
from kumpel_api.rooms.models import RoomFeature
from kumpel_api.rooms.models import RoomImage
from kumpel_api.rooms.models import RoomPlace

# pylint: disable=too-few-public-methods
# pylint: disable=no-self-argument
# pylint: disable=no-self-use
# pylint: disable=unused-argument
# pylint: disable=no-member
# pylint: disable=R0914

# Catalogs


class PlacesCategoryType(DjangoObjectType):
    """Room places category type."""

    class Meta:
        """Properties for the room places categories."""

        model = PlacesCategory
        description = 'Places categories for rooms'
        only_fields = ('id', 'name', 'description')


class FeatureCategoryType(DjangoObjectType):
    """Room features categories type."""

    class Meta:
        """Properties for the features categories."""

        model = FeatureCategory
        description = 'Features categories'
        only_fields = (
            'id',
            'name',
            'description',
        )


class FeatureType(DjangoObjectType):
    """Room features type."""

    category = graphene.Field(
        FeatureCategoryType, description='Category of the feature and details'
    )

    class Meta:
        """Properties for the features of the room."""

        model = Feature
        description = 'Features of the room'
        only_fields = (
            'id',
            'name',
            'icon',
            'is_boolean',
            'description',
            'category',
        )


# Intermediates
class RoomImagesType(DjangoObjectType):
    """Room Images Type."""

    class Meta:
        """Properties for the room images."""

        model = RoomImage
        description = 'Images of the room'
        only_fields = (
            'id',
            'image',
        )


class RoomPlacesType(DjangoObjectType):
    """Room Places Type."""

    category = graphene.Field(
        PlacesCategoryType, description='Details of the place'
    )

    class Meta:
        """Properties for the room places."""

        model = RoomPlace
        description = 'Places near the room'
        only_fields = ('id', 'details', 'category')


class RoomFeaturesType(DjangoObjectType):
    """Room features type."""

    feature = graphene.Field(FeatureType, description='Feature details')

    # def resolve_features(self, args):
    #     """Get feature."""
    #     return self.roomfeature_set.all()

    class Meta:
        """Properties for the room places."""

        model = RoomFeature
        description = 'Features of the room'
        only_fields = ('id', 'feature', 'number')


class RoomType(DjangoObjectType):
    """Room type."""

    images = graphene.List(RoomImagesType, description='Image fields')
    places = graphene.List(RoomPlacesType, description='Places near to room')
    features = graphene.List(
        RoomFeaturesType, description='Features of the room'
    )

    def resolve_images(self, args):
        """Get all image rooms."""
        return self.roomimage_set.all()

    def resolve_places(self, args):
        """Get all places."""
        return self.roomplace_set.all()

    def resolve_features(self, args):
        """Get all features."""
        return self.roomfeature_set.all()

    class Meta:
        """Meta information for RoomType."""

        model = Room
        description = 'Room'
        only_fields = (
            'id',
            'title',
            'description',
            'main_image',
            'images',
            'places',
            'features',
            'host',
            'price',
            'address',
            'latitude',
            'longitude',
            'visibility',
            'availability_date',
        )


# Especials
class RoomFilter(DjangoObjectType):
    """Type for filter room."""

    class Meta:
        """Properties for the type."""

        model = Room
        interfaces = (graphene.relay.Node,)

        filter_fields = {
            'title': ['icontains'],
        }
        description = 'Search room'


class RoomPaginatedType(graphene.ObjectType):
    """Type for paginate Rooms."""

    page = graphene.Int()
    pages = graphene.Int()
    has_next = graphene.Boolean()
    has_prev = graphene.Boolean()
    data = graphene.List(RoomType)

    class Meta:
        """Properties for the type."""

        model = Room
        description = 'Paginate Rooms.'


# Mutations
class PlacesInput(graphene.InputObjectType):
    """Places input to mutation."""

    place = graphene.ID(required=True)
    details = graphene.String(required=True)


class FeaturesInput(graphene.InputObjectType):
    """Places input to mutation."""

    feature = graphene.ID(required=True)
    number = graphene.Int(required=False)


# Mutations
class CreateRoom(graphene.Mutation):
    """Mutation for create a Room."""

    class Arguments:
        """Upload image files."""

        images = Upload(required=True)
        places = graphene.List(PlacesInput, required=True)
        features = graphene.List(FeaturesInput, required=True)
        title = graphene.String(required=True)
        description = graphene.String(required=True)
        address = graphene.String(required=True)
        latitude = graphene.String(required=False)
        longitude = graphene.String(required=False)
        visibility = graphene.Boolean(required=False)
        availability_date = graphene.String(required=False)
        status_id = graphene.Int(required=True)
        price = graphene.Int(required=True)

    room = graphene.Field(RoomType)

    def mutate(root, info, images, places, features, **kwargs):
        """Mutation method."""
        user = None
        try:
            user = info.context.user
            if user.is_anonymous:
                raise Exception('Authentication error. Send a valid token!')
        except ObjectDoesNotExist as error:
            logging.exception(error)

        title = kwargs.get('title', None)
        description = kwargs.get('description', None)
        main_image = images[0]
        host = user.id
        address = kwargs.get('address', None)
        latitude = kwargs.get('latitude', None)
        longitude = kwargs.get('longitude', None)
        visibility = kwargs.get('visibility', None)
        availability_date = kwargs.get('availability_date', None)
        status = kwargs.get('status_id', None)
        price = kwargs.get('price', None)

        room_obj = Room(
            title=title,
            description=description,
            main_image=main_image,
            host_id=host,
            address=address,
            latitude=latitude,
            longitude=longitude,
            visibility=visibility,
            availability_date=availability_date,
            status_id=status,
            price=price,
        )
        room_obj.save()

        # Save image files
        for image in images:
            RoomImage(image=image, room=room_obj).save()
        for place in places:
            category_obj = PlacesCategory.objects.filter(
                id=place.place
            ).first()
            RoomPlace(
                room=room_obj, details=place.details, category=category_obj,
            ).save()
        for feature in features:
            feature_obj = Feature.objects.filter(id=feature.feature).first()
            if feature.number:
                RoomFeature(
                    feature=feature_obj, number=feature.number, room=room_obj
                ).save()
            else:
                RoomFeature(feature=feature_obj, room=room_obj).save()
        return CreateRoom(room=room_obj)


class DeleteRoom(graphene.Mutation):
    """Mutation for delete a Room."""

    class Arguments:
        """Arguments for delete a Room."""

        room_id = graphene.Int(required=True)

    room = graphene.Field(RoomType)

    def mutate(root, info, **kwargs):
        """Mutation method."""
        user = None
        try:
            user = info.context.user
            if user.is_anonymous:
                raise Exception('Authentication error. Send a valid token!')
        except ObjectDoesNotExist as error:
            logging.exception(error)

        room = kwargs.get('room_id', None)

        room_obj = Room.objects.get(pk=room)
        room_obj.delete()

        return DeleteRoom(room=room_obj)


class UnDeleteRoom(graphene.Mutation):
    """Mutation for delete a Room."""

    class Arguments:
        """Arguments for delete a Room."""

        room_id = graphene.Int(required=True)

    room = graphene.Field(RoomType)

    def mutate(root, info, **kwargs):
        """Mutation method."""
        user = None
        try:
            user = info.context.user
            if user.is_anonymous:
                raise Exception('Authentication error. Send a valid token!')
        except ObjectDoesNotExist as error:
            logging.exception(error)
        room = kwargs.get('room_id', None)

        room_obj = Room.objects.get(pk=room)
        room_obj.undelete()

        return UnDeleteRoom(room=room_obj)
