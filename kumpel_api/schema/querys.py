"""API Querys.

Create all querys for API.
"""
import logging

import graphene
from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q

from kumpel_api.rooms.models import Feature
from kumpel_api.rooms.models import FeatureCategory
from kumpel_api.rooms.models import PlacesCategory
from kumpel_api.rooms.models import Room
from kumpel_api.schema.type_favorite_rooms import FavoriteRoomPaginatedType
from kumpel_api.schema.type_room import FeatureCategoryType
from kumpel_api.schema.type_room import FeatureType
from kumpel_api.schema.type_room import PlacesCategoryType
from kumpel_api.schema.type_room import RoomPaginatedType
from kumpel_api.schema.type_room import RoomType
from kumpel_api.schema.type_user import KumpelRoleType
from kumpel_api.schema.type_user import UserType
from kumpel_api.utils.utils import get_paginator

# pylint: disable=too-few-public-methods
# pylint: disable=no-self-argument
# pylint: disable=no-self-use
# pylint: disable=unused-argument
# pylint: disable=no-member


# Query
class Query:
    """Queries For Kampel Api."""

    kumpel_roles = graphene.List(
        KumpelRoleType, description='Kumpel roles for register on APP'
    )

    user = graphene.Field(
        UserType,
        description='Get autenticated user data. Add header. Authorization: JWT {token}',
    )

    rooms = graphene.Field(
        RoomPaginatedType,
        page=graphene.Int(required=False),
        limit=graphene.Int(required=False),
        description='Get all paginated rooms',
    )

    rooms_host = graphene.Field(
        RoomPaginatedType,
        page=graphene.Int(required=False),
        limit=graphene.Int(required=False),
        description='Get all rooms by host',
    )

    rooms_favorites = graphene.Field(
        FavoriteRoomPaginatedType,
        page=graphene.Int(),
        limit=graphene.Int(),
        description='Get favorites rooms of a user',
    )

    rooms_search = graphene.Field(
        RoomPaginatedType,
        text_to_search=graphene.String(required=True),
        page=graphene.Int(),
        limit=graphene.Int(),
        description='Search rooms on Kumpel',
    )

    rooms_features_categories = graphene.List(
        FeatureCategoryType, description='List of feature categories',
    )

    rooms_features = graphene.List(
        FeatureType, description='List of room features',
    )

    rooms_places_categories = graphene.List(
        PlacesCategoryType, description='List of places categories',
    )

    room = graphene.Field(
        RoomType,
        room_id=graphene.Int(required=True),
        description='Get Room by id',
    )

    # pylint: disable=unused-argument,no-self-use

    def resolve_user(self, info):
        """Query a single profile."""
        user = None
        try:
            user = info.context.user
            if user.is_anonymous:
                raise Exception('Authentication error. Send a valid token!')
        except ObjectDoesNotExist as error:
            logging.exception(error)

        return user

    def resolve_kumpel_roles(self, info):
        """Query a single profile."""
        return Group.objects.exclude(name='Spartans')

    def resolve_rooms(self, info, page=1, limit=5, **kwargs):
        """Rooms paginated."""
        if limit > 50:
            limit = 50
        rooms = Room.objects.all()

        return get_paginator(rooms, limit, page, RoomPaginatedType)

    def resolve_rooms_host(self, info, page=1, limit=5, **kwargs):
        """Rooms paginated."""
        rooms = None
        if limit > 50:
            limit = 50
        try:
            user = info.context.user
            if user.is_anonymous:
                raise Exception('Authentication error. Send a valid token!')
            rooms = user.room_set.all()
        except ObjectDoesNotExist as error:
            logging.exception(error)

        return get_paginator(rooms, limit, page, RoomPaginatedType)

    def resolve_room(self, info, room_id):
        """Query a single room."""
        return Room.objects.get(pk=room_id)

    def resolve_rooms_search(self, info, text_to_search, page=1, limit=5):
        """Query search rooms."""
        print(text_to_search)
        rooms = Room.objects.filter(
            Q(title__icontains=text_to_search)
            | Q(description__icontains=text_to_search)
        )

        return get_paginator(rooms, limit, page, RoomPaginatedType)

    def resolve_rooms_favorites(self, info, page=1, limit=5):
        """Query to get all favorites rooms of a user."""
        favorite_rooms = None
        try:
            user = info.context.user
            if user.is_anonymous:
                raise Exception('Authentication error. Send a valid token!')
            favorite_rooms = user.favoriteroom_set.all()
        except ObjectDoesNotExist as error:
            logging.exception(error)
        return get_paginator(
            favorite_rooms, limit, page, FavoriteRoomPaginatedType
        )

    def resolve_rooms_features(self, info):
        """Query to get all features."""
        return Feature.objects.all()

    def resolve_rooms_features_categories(self, info):
        """Query to get all features categories."""
        return FeatureCategory.objects.all()

    def resolve_rooms_places_categories(self, info):
        """Query to get all features categories."""
        return PlacesCategory.objects.all()
