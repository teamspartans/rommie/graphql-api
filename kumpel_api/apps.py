"""AppConfigs."""
from django.apps import AppConfig


# pylint: disable=too-few-public-methods
class KumpelApiConfig(AppConfig):
    """App Configurations."""

    name = 'kumpel_api'
