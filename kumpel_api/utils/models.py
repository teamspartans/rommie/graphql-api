"""Generic Models for many Entities."""
from django.db import models
from safedelete.config import DELETED_VISIBLE_BY_FIELD
from safedelete.managers import SafeDeleteManager


class KumpelBaseModel(models.Model):
    # pylint: disable=too-few-public-methods
    """Kumpel Base Model.

    Kumpel acts as an abstract base class from which every other model in the project
    will inherit. This class provides every table with the following attributes:
        - created (DateTime) = Store the datetime the object was created.
        - modified (DateTime) = Store the las datetime the object was modified
    """

    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time on which the object was created.',
    )

    modified = models.DateTimeField(
        'modified at',
        auto_now=True,
        help_text='Date time on which the object was last modified.',
    )

    class Meta:
        """Meta Options.

        Put the class as abstract and put some options.
        """

        abstract = True

        get_latest_by = 'created'
        ordering = ['-created', '-modified']


class SoftDeleteVisibilityManager(SafeDeleteManager):
    """Manager for Soft Delete Visilibity."""

    # pylint: disable=too-few-public-methods
    _safedelete_visibility = DELETED_VISIBLE_BY_FIELD
    _safedelete_visibility_field = 'pk'
