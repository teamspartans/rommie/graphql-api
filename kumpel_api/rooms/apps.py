"""Rooms App."""
from django.apps import AppConfig

# pylint: disable=too-few-public-methods


class RoomsAppConfig(AppConfig):
    """Rooms App Config."""

    name = 'kumpel_api.rooms'
    verbose_name = 'Rooms'
