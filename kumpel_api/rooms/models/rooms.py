"""Rooms Model."""
from django.conf import settings
from django.db import models
from django.utils.safestring import mark_safe
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE

from kumpel_api.rooms.models.feature import Feature
from kumpel_api.rooms.models.status import Status
from kumpel_api.users.models.users import User
from kumpel_api.utils.models import KumpelBaseModel
from kumpel_api.utils.models import SoftDeleteVisibilityManager


# pylint: disable=too-few-public-methods


class Room(KumpelBaseModel, SafeDeleteModel):
    """Room Model.

    Rooms that users offer for rent.
    """

    _safedelete_policy = SOFT_DELETE_CASCADE
    objects = SoftDeleteVisibilityManager()

    title = models.CharField(
        'title', max_length=80, blank=False, null=False, help_text='Room Title'
    )

    description = models.CharField(
        max_length=200, blank=False, null=False, help_text='Room Description'
    )

    main_image = models.ImageField(
        'main image',
        upload_to='rooms/images/',
        blank=True,
        null=True,
        help_text='Main Image of the Room',
    )

    host = models.ForeignKey(
        User, on_delete=models.CASCADE, help_text='Room Owner'
    )

    status = models.ForeignKey(
        Status, on_delete=models.PROTECT, help_text='Status Room'
    )

    price = models.FloatField(
        blank=True, default=0, help_text='Price to rent room'
    )

    address = models.CharField(
        max_length=250, blank=False, null=False, help_text='Room Address'
    )

    latitude = models.CharField(
        max_length=20, blank=True, null=True, help_text='Room latitude'
    )

    longitude = models.CharField(
        max_length=20, blank=True, null=True, help_text='Room longitude'
    )

    visibility = models.BooleanField(
        default=False, help_text='Room visiblity for all users'
    )

    features = models.ManyToManyField(
        Feature, help_text='Room Features', through='RoomFeature'
    )

    availability_date = models.DateTimeField(
        help_text='Availability Room. Date Field Format: YYYY-MM-DD HH:MM[:ss[.uuuuuu]',
        null=True,
        blank=True,
    )

    def __str__(self):
        """Return room title."""
        return self.title

    def image_tag(self):
        """Create HTML picture user for display on apps."""
        if self.main_image:
            return mark_safe(  # nosec - ✅ show image on admin
                f'<img src="{settings.MEDIA_URL}{self.main_image}" style="width: 50px;" />'
            )
        return 'Image not found'

    def get_host_name(self):
        """Get host name."""
        return self.host.get_full_name()

    class Meta(KumpelBaseModel.Meta):
        """Meta options for Rooms."""

        # pylint: disable=too-few-public-methods
        db_table = 'ROOMS'
