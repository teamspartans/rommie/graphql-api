"""Status Model for Kumpel API."""
from colorfield.fields import ColorField
from django.db import models

from kumpel_api.utils.models import KumpelBaseModel


class Status(KumpelBaseModel):
    """Status Model."""

    # pylint: disable=too-few-public-methods
    status = models.CharField(
        max_length=30, unique=True, help_text='Rooms status'
    )

    icon = models.CharField(
        max_length=100, null=True, blank=True, help_text='Icon for the Status'
    )

    color = ColorField(
        'color',
        default='#FF0000',
        null=True,
        blank=True,
        help_text='Personalized Color Type.',
    )

    def __str__(self):
        """Return name of the status."""
        return self.status

    class Meta(KumpelBaseModel.Meta):
        """Meta options for Status Model."""

        # pylint: disable=too-few-public-methods
        db_table = 'STATUS'
        verbose_name_plural = 'Status'
