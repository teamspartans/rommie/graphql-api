"""Feature Category model for Kumpel API."""
from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE

from kumpel_api.utils.models import KumpelBaseModel


class FeatureCategory(KumpelBaseModel, SafeDeleteModel):
    """Feature Category Model."""

    # pylint: disable=too-few-public-methods

    _safedelete_policy = SOFT_DELETE

    name = models.CharField(
        max_length=40, null=False, blank=False, help_text='Category Name'
    )

    description = models.CharField(
        max_length=200, null=True, blank=True, help_text='Category Description'
    )

    def __str__(self):
        """Return name of the category."""
        return self.name

    class Meta(KumpelBaseModel.Meta):
        """Meta options for Feature Category."""

        # pylint: disable=too-few-public-methods

        db_table = 'FEATURE_CATEGORIES'
        verbose_name_plural = 'Feature Categories'
