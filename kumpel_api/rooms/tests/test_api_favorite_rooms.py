# """Tests for Favorites Rooms from Kumpel API."""
# from graphene_django.utils.testing import GraphQLTestCase
# from core.schema import schema
# from kumpel_api.rooms.models.favorite_rooms import FavoriteRoom
# from kumpel_api.rooms.models.rooms import Room
# from kumpel_api.rooms.models.status import Status
# from kumpel_api.users.models.users import User
# from kumpel_api.utils.utils import DataGenerator
# class FavoritesRoomsTestCase(GraphQLTestCase):
#     """Favorites Rooms Tests API."""
#     GRAPHQL_SCHEMA = schema
#     GRAPHQL_URL = '/graphql'
#     # pylint: disable=invalid-name
#     generator = DataGenerator()
#     generator = generator.fake
#     def setUp(self):  # noqa: D401
#         """Setup for tests."""
#         self._user = User.objects.create(
#             email=self.generator.email(),
#             first_name=self.generator.first_name(),
#             last_name=self.generator.last_name(),
#             phone_number=self.generator.phone_number(),
#             password=self.generator.password(length=17),
#         )
#         self._room_status = Status.objects.create(
#             status=self.generator.text(max_nb_chars=17)
#         )
#         self._room = Room.objects.create(
#             host_id=self._user.id,
#             title=self.generator.text(max_nb_chars=17),
#             description=self.generator.text(max_nb_chars=17),
#             address=self.generator.address(),
#             status_id=self._room_status.id,
#         )
#         self._room = Room.objects.create(
#             host_id=self._user.id,
#             title=self.generator.text(max_nb_chars=17),
#             description=self.generator.text(max_nb_chars=17),
#             address=self.generator.address(),
#             status_id=self._room_status.id,
#         )
#         self._favorite_room = FavoriteRoom.objects.create(
#             user_id=self._user.id, room_id=self._room.id
#         )
# def test_search_favorite_room_work(self):
#     """Verify that query for search favorites rooms of a user works."""
#     response = self.query(
#         """
#             {
#                 roomsFavorites(){
#                     data{
#                         id
#                         user {
#                             fullName
#                         }
#                         room{
#                             title
#                         }
#                     }
#                 }
#             }
#         """
#     )
#     self.assertResponseNoErrors(response)
# def test_create_a_favorite_room_work(self):
#     """Verify that query for search favorites rooms of a user works."""
#     response = self.query(
#         """
#             mutation {
#                 roomFavoritesAdd(userId: 1, roomId: 1) {
#                     favoriteRoom {
#                         id
#                         user {
#                             id
#                         }
#                         room {
#                             title
#                         }
#                     }
#                 }
#             }
#         """
#     )
#     self.assertResponseNoErrors(response)
