"""Room admin config."""
from django.contrib import admin

from .models import FavoriteRoom
from .models import Feature
from .models import FeatureCategory
from .models import PlacesCategory
from .models import Room
from .models import RoomFeature
from .models import RoomImage
from .models import RoomPlace
from .models import Status

# pylint: disable=too-few-public-methods


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    """Configuration for Kumpel model.

    Room admin configs.
    """

    # List of element can display on admin
    list_display = (
        'title',
        'image_tag',
        'get_host_name',
        'status',
        'availability_date',
        'visibility',
    )

    # Some filters and configurations
    date_hierarchy = 'created'
    search_fields = ['title', 'description']
    list_filter = ['visibility']
    list_per_page = 10


@admin.register(FavoriteRoom)
class FavoriteRoomAdmin(admin.ModelAdmin):
    """Configuration for Kumpel model.

    Favorite Room admin configs.
    """

    # List of element can display on admin
    list_display = (
        'user',
        'room',
        'created',
    )

    # Some filters and configurations
    date_hierarchy = 'created'
    list_per_page = 10


@admin.register(RoomImage)
class RoomImageAdmin(admin.ModelAdmin):
    """Configuration for Kumpel model.

    Room Image admin configs.
    """

    # List of element can display on admin
    list_display = (
        'image_tag',
        'room',
        'created',
    )

    # Some filters and configurations
    date_hierarchy = 'created'
    list_per_page = 10


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    """Configuration for Kumpel model.

    Status admin configs.
    """

    # List of element can display on admin
    list_display = (
        'status',
        'icon',
        'color',
    )

    # Some filters and configurations
    date_hierarchy = 'created'
    list_per_page = 10


@admin.register(RoomPlace)
class RoomPlaceAdmin(admin.ModelAdmin):
    """Configuration for Kumpel model.

    Place admin configs.
    """

    # List of element can display on admin
    list_display = ('room', 'category')

    # Some filters and configurations
    date_hierarchy = 'created'
    list_per_page = 10


@admin.register(RoomFeature)
class RoomFeatureAdmin(admin.ModelAdmin):
    """Configuration for Kumpel model.

    Place admin configs.
    """

    # List of element can display on admin
    list_display = ('id', 'number', 'room', 'feature')

    # Some filters and configurations
    date_hierarchy = 'created'
    list_per_page = 10


@admin.register(FeatureCategory)
class FeatureCategoryAdmin(admin.ModelAdmin):
    """Configuration for Kumpel model.

    Feature Category admin configs.
    """

    # List of element can display on admin
    list_display = (
        'name',
        'description',
    )

    # Some filters and configurations
    date_hierarchy = 'created'
    list_per_page = 10


@admin.register(Feature)
class FeatureAdmin(admin.ModelAdmin):
    """Configuration for Kumpel model.

    Feature admin configs.
    """

    # List of element can display on admin
    list_display = ('name', 'is_boolean')

    # Some filters and configurations
    date_hierarchy = 'created'
    list_per_page = 10


@admin.register(PlacesCategory)
class PlacesCategoryAdmin(admin.ModelAdmin):
    """Configuration for Kumpel model.

    PlacesCategory admin configs.
    """

    # List of element can display on admin
    list_display = ('name', 'description')

    # Some filters and configurations
    date_hierarchy = 'created'
    list_per_page = 10
