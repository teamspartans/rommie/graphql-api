"""Users App."""
from django.apps import AppConfig

# pylint: disable=too-few-public-methods


class UsersAppConfig(AppConfig):
    """Users App Config."""

    name = 'kumpel_api.users'
    verbose_name = 'Users'
