"""Models User Init."""
from .genres import Genre
from .hobbies import Hobby
from .interests import Interest
from .profiles import Profile
from .users import User
