"""Hobbies model for Kumpel API."""
from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE

from kumpel_api.utils.models import KumpelBaseModel


class Hobby(KumpelBaseModel, SafeDeleteModel):
    """User Hobby model."""

    # pylint: disable=too-few-public-methods

    _safedelete_policy = SOFT_DELETE

    name = models.CharField(
        max_length=50, null=False, blank=False, help_text='Hobby Name'
    )

    description = models.CharField(
        max_length=200, null=True, blank=True, help_text='Hobby Description'
    )

    def __str__(self):
        """Return name of the Hobby."""
        return self.name

    class Meta(KumpelBaseModel.Meta):
        """Meta options for Hobby Model."""

        # pylint: disable=too-few-public-methods

        db_table = 'HOBBIES'
        verbose_name_plural = 'Hobbies'
