"""User Model."""
from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE

from kumpel_api.utils.models import KumpelBaseModel


class User(KumpelBaseModel, AbstractUser, SafeDeleteModel):
    """User model.

    Extends from Django's Abstract user, change the username field to
    email and add some extra fields.
    """

    _safedelete_policy = SOFT_DELETE_CASCADE

    # pylint: disable=no-self-use

    # Email
    email = models.EmailField(
        'email address',
        unique=True,
        help_text='Unique email for user',
        error_messages={'unique': 'A user with that email already exists.'},
    )

    # First Name
    first_name = models.CharField(
        max_length=255, help_text='User First Name', null=False, blank=False
    )

    # Last Name
    last_name = models.CharField(
        null=False, blank=False, max_length=255, help_text='User Last Name'
    )

    username = models.CharField(
        null=True,
        blank=True,
        default='',
        max_length=255,
        unique=False,
        help_text='Username',
    )

    # Phone
    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
        message='Phone number must be entered in the format: +999999999. Up to 15 digits allowed. ',
    )

    phone_number = models.CharField(
        validators=[phone_regex],
        max_length=17,
        blank=False,
        null=False,
        help_text='User Phone Number',
    )

    # Is Verified
    is_verified = models.BooleanField(
        'verified',
        default=False,
        help_text='Set to true when the user have verified its email address.',
    )

    # Valid if user is host or guest user
    is_client = models.BooleanField(
        'client',
        default=False,
        help_text=('Help easily distinguish users and perform queries.'),
    )

    # Fields Settings
    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    def __str__(self):
        """Return the email of the user."""
        return self.email

    def get_full_name(self):
        """Get full name."""
        return self.first_name + ' ' + self.last_name

    def get_user_fields(self):
        """User fields."""
        return self

    def get_short_name(self):
        """Get a user shortname (email)."""
        return self.username

    class Meta(KumpelBaseModel.Meta):
        """Meta options for Users."""

        # pylint: disable=too-few-public-methods
        db_table = 'USERS'
