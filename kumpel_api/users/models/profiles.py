"""User Profile Model."""
from django.conf import settings
from django.db import models
from django.utils.safestring import mark_safe
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE_CASCADE

from kumpel_api.users.models.genres import Genre
from kumpel_api.users.models.hobbies import Hobby
from kumpel_api.users.models.interests import Interest
from kumpel_api.utils.models import KumpelBaseModel


# pylint: disable=R0903


class Profile(KumpelBaseModel, SafeDeleteModel):
    """User Profile.

    A profile holds a user's public data like interests, picture and statistics.


    Args:
        models.Model: Extend the KumpelBaseModel Model.
    """

    _safedelete_policy = SOFT_DELETE_CASCADE

    # User
    user = models.OneToOneField(
        'users.User',
        on_delete=models.CASCADE,
        help_text='User information',
        null=False,
    )

    # Genre
    genre = models.ForeignKey(
        Genre,
        on_delete=models.PROTECT,
        null=False,
        help_text='Genre of the user',
    )

    hobbies = models.ManyToManyField(
        Hobby, db_table='USERS_HOBBIES', help_text='User Hobbies'
    )

    interests = models.ManyToManyField(
        Interest, db_table='USERS_INTERESTS', help_text='User Interests'
    )

    # Picture Field
    picture = models.ImageField(
        upload_to='users/pictures',
        blank=True,
        null=True,
        help_text='Profile Avatar',
    )

    # Birthday
    birthday = models.DateField(
        blank=True, null=True, help_text='Birthday of the User'
    )

    def __str__(self):
        """Return Username."""
        return str(self.user)

    def image_tag(self):
        """Create HTML picture user for display on apps."""
        if self.picture:
            return mark_safe(  # nosec - ✅ show image on admin
                f'<img src="{settings.MEDIA_URL}{self.picture}" style="width: 50px;" />'
            )
        return 'Image not found'

    def full_name(self):
        """Get full name."""
        return self.user.get_full_name()

    class Meta(KumpelBaseModel.Meta):
        """Meta options for Profiles."""

        db_table = 'USERS_PROFILES'
