"""Interest model for Kumpel API."""
from django.db import models
from safedelete.models import SafeDeleteModel
from safedelete.models import SOFT_DELETE

from kumpel_api.utils.models import KumpelBaseModel


class Interest(KumpelBaseModel, SafeDeleteModel):
    """User Interest model."""

    # pylint: disable=too-few-public-methods

    _safedelete_policy = SOFT_DELETE

    name = models.CharField(
        max_length=50, null=False, blank=False, help_text='Interest Name'
    )

    description = models.CharField(
        max_length=200, null=True, blank=True, help_text='Interest Description'
    )

    def __str__(self):
        """Return name of the Interest."""
        return self.name

    class Meta(KumpelBaseModel.Meta):
        """Meta options for Interest Model."""

        # pylint: disable=too-few-public-methods

        db_table = 'INTERESTS'
        verbose_name_plural = 'Interests'
