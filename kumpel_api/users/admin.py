"""Users Admin Configuration."""
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models.genres import Genre
from .models.hobbies import Hobby
from .models.interests import Interest
from .models.profiles import Profile
from .models.users import User

# pylint: disable=too-few-public-methods


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    """Configuration for Kumpel model.

    Profile admin configs.
    """

    # List of element can display on admin
    list_display = (
        'full_name',
        'image_tag',
        'user',
        'birthday',
    )

    # Some filters and configurations
    date_hierarchy = 'created'
    list_filter = ['user']
    search_fields = ['user__first_name']
    list_per_page = 10


class CustomUserAdmin(UserAdmin):
    """Configuration for Kumpel model.

    User admin configs.
    """

    list_display = (
        'email',
        'username',
        'first_name',
        'phone_number',
        'last_name',
        'is_staff',
        'is_verified',
    )

    list_filter = ('is_verified', 'is_staff', 'created', 'modified')

    # Some filters and configurations
    date_hierarchy = 'created'
    search_fields = ['user']
    list_per_page = 10


class GenreAdmin(admin.ModelAdmin):
    """Configuration for kumpel model.

    Genre admin configs.
    """

    list_display = ('name',)
    list_filter = ('name', 'created', 'modified')
    date_hierarchy = 'created'
    list_per_page = 10


class HobbyAdmin(admin.ModelAdmin):
    """Configuration for kumpel model.

    Hobby admin configs.
    """

    list_display = ('name', 'description')
    date_hierarchy = 'created'
    list_per_page = 10


class InterestAdmin(admin.ModelAdmin):
    """Configuration for kumpel model.

    Interest admin configs.
    """

    list_display = ('name', 'description')
    date_hierarchy = 'created'
    list_per_page = 10


# Register models and configurations
admin.site.register(User, CustomUserAdmin)
admin.site.register(Genre, GenreAdmin)
admin.site.register(Hobby, HobbyAdmin)
admin.site.register(Interest, InterestAdmin)
