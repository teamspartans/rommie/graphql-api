"""Urls for kumpel project.

Link to admin, show dashboard and main page.
"""
from django.contrib.auth.decorators import login_required
from django.urls import path

from .views import catalogs
from .views import clear_data
from .views import generate_rooms
from .views import generate_users
from .views import main_page
from .views import reduce_rooms_images
from .views import reduce_avatar_images

urlpatterns = [
    path('', main_page),
    path('test/clear_data', login_required(clear_data)),
    path('test/catalogs', login_required(catalogs)),
    path('test/generate/users', login_required(generate_users)),
    path('test/generate/rooms', login_required(generate_rooms)),
    path('test/reduce/rooms', login_required(reduce_rooms_images)),
    path('test/reduce/avatars', login_required(reduce_avatar_images)),
]
